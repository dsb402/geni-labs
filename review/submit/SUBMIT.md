
Project Review
=====================================================

Please fill in your name and net ID in the table below.

Lab Assignment       | Project Review
-------------------- | --------------------------------
Name                 | Deepali Bhanushali
Net ID               | dsb402	
Assigned project (#) | 001
Review due           | Monday, 4 May 11:59PM


Please write detailed comments answering the questions below.
Do not just answer "yes" or "no" - comment on *how well* the authors
address each of these aspects of experimentation, and offer 
suggestions for improvement.

## Overview

1) Briefly summarize the experiment in this project.
The experiment is basically devided into two parts.
first part measures packet delay and packet loss of UDP traffic by varying queue size.
second part studies the performance of UDP based data transfer(UDT) with reference to FTP under different
bandwidth,delay and packet loss probabilities.



2) Does the project generally follow the guidelines and parameters we have 
learned in class? 
The project does not follow all the guidelines and parameters we have learned in the class.
Below are mentioned few things that   prject meets and does not meet.
Goal is specific and meaningful from which interesting results can be drawn.
Rspec file or topology used in the experiment is not given hence methdology is ambiguous.
Metrics and parameters are selected reasonably.
Visual representation of the results is there but steps to visually represent the results 
are not documented.
Reproducibility of the experiment is not guaranteed as topology is not known explicitly.


## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?
There are two goals of this experiment. 
First goal is to measure packet loss and packet delay of UDP traffic for varying queue length.
Second goal is to study performance of UDP based file transfer(UDT) with  reference to FTP under different
bandwidth,delay and packet loss probabilities.
It is useful and have interesting results as it gives out logical conclusion on the packet delay and packet loss of UDT 
traffic with varying queue length.
Second goal also gives out interesting results about speed of file transfer in case of UDP based and FTP based file transfer.


2) Are the metric(s) and parameters chosen appropriate for this 
experiment goal? Does the experiment design clearly support the experiment goal?
Packet delay, packet loss and queue length are appropriate metrics and parameters for this experiment as experiment it clearly 
supports the experiment goal that is to study the effect of variation in queue length on packet delay and packet loss of UDP traffic.
For the second part of the experiment, speed of transmission is appropriate metric as it is according to the goal of the experiment.
Bandwidth, delay and packet loss probability are well chosen parameters as well as per the goal of the experiment.


3) Is the experiment well designed to obtain maximum information with the 
minimum number of trials?
First part of the experiment is implemented in five simple steps which leads to a logical conclusion.
Each of these steps include adjust the queue size, get the packet loss and packet delay.
Second part of the experiment has two simple steps of adjusting the bandwidth , delay and packet loss probability followed by
measuring the speed of transmission in case of UDP based and FTP based file transfer respectively.
Hence I believe the experiment is well designed to obtain maximum information with minimum number of trials as unnecessary
trials are avoided.


4) Are the metrics selected for study the *right* metrics? Are they clear, 
unambiguous, and likely to lead to correct conclusions? Are there other 
metrics that might have been better suited for this experiment?
Packet delay and packet loss are right metrics as the goal of first part of the experiment is specifically to study the effect of
varying queue length on Packet delay and packet loss on UDP traffic.
speed of transmission is the appropriate metric for second part of the experiment as the goal of the second part is to study the
performance of UTP based file transfer with reference to TCP based file transfer.

No other metrics but packet delay, packet loss and speed of transmission are best suited for this experiment.


5) Are the parameters of the experiment meaningful? Are the ranges 
over which parameters vary meaningful and representative?
Queue length is the meaningful parameter for this experiment. Queue length has been chosen to be varied in the
range of 20kb,1mb,2mb,10mb,1000mb(20kb to 1000mb).
Hence the experiment takes into account very small to moderate to large queue length into account to do the measurements.
In my view this range is representative enough to get the meaningful results to conclude about packet delay and packet loss.
Range of parameters in the second part of the experiment is ambiguous as expalanation does not go well with the final table
to quantitatively conclude the results.
But range of varying parameters takes into account limited bandwith to unlimited bandwidth, small delay to no delay. 
This is pretty sufficient to comment on the speed of file transfer in case of UDP based and FTP based file transfer.


6) Have the authors sufficiently addressed the possibility of interactions 
between parameters?
The experiment does not mention any possibility of interaction between the parameters.
It is not required since the parameters are independent.



7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate 
and realistic?
There are not any comparisons in first part of the experiment.
Second part involves comparison between speed of file transfer in case of UDP based and FTP based file transfer.
I could see some sort of manipulation or misrepresentation or mistake while representing the comparisons quantitatively as the
results obtained does not lead exactly to the final table given in the experiment.
But if we just refer the final tabel baseline for comparisons is appropriate and realistic which is speed of file transfer for
different combination of bandwidth, delay and packet loss probability in case of UDP based and FTP based file transfer.





## Communicating results


1) Do the authors report the quantitative results of their experiment?
The authors report number of packets dropped and maximum delay. These are the quantitative results of part 1 of the experiment.
For the second part of the experiment, quantitative results are communicated in tabular format.



2) Is there information given about the variation and/or distribution of 
experimental results?
 The experiment satisfactorily states the results logically but they do not provide information about variation
 of experimental results in miscellaneous or different environment than the setup used in the experiment. 
 For example when you change the topology or use LFN network.


3) Do the authors practice *data integrity* - telling the truth about their data, 
avoiding ratio games and other practices to artificially make their results seem better?
I do not find anything that is used to manupulate the data. There are no ratio games and other practices to artificially
make their results seem better in the first part of the experiment.
for the first part, Results are quantitatively stated in terms of Number of packet lost and maximum packet delay.
Hence I believe authors have been successful in practicing "data integrity" in first half.
As mentioned previously, I believe there is some sort of manipulation or misrepresentation or mistake while representing 
the comparisons quantitatively as the results obtained does not lead exactly to the final table given in the experiment.


4) Is the data presented in a clear and effective way? If the data is presented in 
graphical form, is the type of graph selected appropriate for the "story" that 
the data is telling? 

Data is presented in a clear and effective way in graphical form and the type of graph chosen is approprate
for desplaying results in my opinion as it clearly shows the behaviour/trend of packet delay and packet loss 
with respect to queue size which is the main aim of the experiment.
For the second part data representaion is in tabular form. Authors have not used any graphical form.

5) Are the conclusions drawn by the authors sufficiently supported by the 
experiment results?
The experiment results show that packet loss is inversely proportinal to queue length and packet delay is directly
proportional to queue length. As it is evident from the concluson of the experiment that concusion drawn is clealy
supported by the experiment results.


## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results, 
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?
Authors failed to include the instructions for reproducing the experiment in these 3 ways.
As there is no Raw data that has been shared so that the quantitative results can be processed and visualised.
There is no topology or Experiment setup(Rspec file or live setup) that has been shared.
The other steps to get the quantitative results to obtain the data is given.
But again there is no mention of instruction to visualise the quantitative data obtained.
There are many things which needs to be assumed while reproducing the experiment like topology used,initial configuration of router,
client and server.


2) Were you able to successfully produce experiment results? 
I was able to successfully produce results of part 1 of the experiment after I could predict the topology but I could not reproduce the final table
of conclusion for second part of the experiment because the instructions and explanation to reproduce did not match the conclusion.
The instructions and supporting information seemed ambiguous. As per the instructions speed of transmision was measured for utp and ftp based traffic
by varying parametres bandwidth,delay and probability packet of loss from 1000 mb , 1ms , 0.000 to 0mb,0,0.001(unlimited bandwidth). But in table that 
logically concludes second part of the experiment has these same set of parameters in different combination.
So I would say that I could partially reproduce the experiment results.



3) How long did it take you to run this experiment, from start to finish?
It took me about 9 to 10 hours to run this experiment, from start to finish that includes to allocate the resources and get the results of part1 and part 2 of the experiment.


4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
 There was not rspec file or experimental setup/topology shared for the first part of the experiment. So I read through the methodology and steps givem to reproduce the experiment carefully.
 From the methodoilogy and instructions, I could predict that the topology that could have been used by the authors can be a simple topololgy consisting of a client (that they have named source in the experiment), a router in between and a server(authors 
 have named it destination in the experiment).
 I processed raw data using Rscript that I made to represent the results graphically.


5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. 
How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
 In my view, this experiment falls in third degree of reproducibility as the researcher may need to spend quite some time and effort to figure the topology which may require 
 some trial and error technique as well. 
 Instructions given to get the raw data are pretty straight forward and easy to implement. 
 So we can get the raw data. but again there are no instructions given for generating graphs from the raw data.
 So we are unaware of the tools and methodology used by the authors to visually represent the data.
 I could see that there has been a mistake while documenting the figures for second part of the experiment.
 Figures of FTP based file transfer has been erroneously kept in UDT based file transfer.
 

## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

I think it would have been a good idea if the authors have shared the Resource specification file or live setup to make the task of reproducing the experiment easier.
This would definitelty help improve degree of reproducibility as reviewers do not have to spend any time and effort figure out the topology.
Includiong raw data files would have been much better for ease of reproducibility.
Instructions on how they processed raw data to visualise their results should be considered.
Instructions to reproduce the second part of the experiment should have been more clear as the instructions and figures diven do not match the final table that colcludes 
second part of the experiment.
using Bar graph to visulise the difference in speed of transmission would be a good option to communcate the results more clearly.

I recommend above things because according to me this may mprove degree of reproducibility of the experiment from 3 to 2.













