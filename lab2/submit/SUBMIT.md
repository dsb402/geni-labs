
Lab 2: Flow and Congestion Control/Experiment Design
=====================================================

Please fill in your name and net ID in the table below.

Lab Assignment | 2 - Flow and Congestion Control
-------------- | --------------------------------
Name           |Deepali Bhanushali
Net ID         |dsb402
Report due     | Sunday, 22 February 11:59PM


Please answer the following questions:

1) Describe (as specifically as possible) the *goal* of your
experiment. What makes this a good goal? Do not just repeat
all of the indicators of a "good" goal described in the lecture;
explain *how* your selected goal embodies those indicators.

Goal of my experiment is to study TCP variant cubic by observing its cwnd and throughput behaviour w.r.t time for different RTT. 


2) What is the TCP congestion control variant you have selected
as the main subject of your study? Describe briefly (1 paragraph)
what characterizes this TCP congestion control, and how it works.
Cite your sources.

Ans: Cubic is the TCP congestion control variant that i have selected as the main subject of my study.
The window growth function of CUBIC is governed by a cubic function in terms of the elapsed time since
the last loss event. Our experience indicates that the cubic function provides a good stability and
scalability. Furthermore, the real-time nature of the protocol keeps the window growth rate independent
of RTT, which keeps the protocol TCP friendly under both short and long RTT paths.
source:http://www4.ncsu.edu/~rhee/export/bitcp/cubic-paper.pdf


3) What is the other LFN TCP you have selected to use in your
experiment? What is the non-LFN TCP you have selected to use
in your experiment? Be brief (1 paragraph each), and cite your
sources. Why did you choose these specifically?
Other LFN TCP that I have selected is High Speed.
Other non-LFN TCP that I have selected is reno.

High Speed:
Advantage:TCP Reno increases its rate too slowly and decreases it too fast.Rise faster and come down slower than Reno
When an ACK is received (in congestion avoidance), the window is increased by a(w)/w and when a loss is detected
through triple duplicate acknowledgments, the window is decreased by (1-b(w))w, where w is the current window size.
When the congestion window is small, HSTCP behaves exactly like standard TCP so a(w) is 1 and b(w) is 0.5.
When TCP's congestion window is beyond a certain threshold, a(w) and b(w) become functions of the current window size. 
In this region, as the congestion window increases, the value of a(w) increases and the value of b(w) decreases.
This means that HSTCP's window will grow faster than standard TCP and also recover from losses more quickly. 
source:http://research.microsoft.com/en-us/um/redmond/events/tcpsummit/slides/raj_jain_high_speed_tcp.pdf
source:http://en.wikipedia.org/wiki/HSTCP

Reno:TCP Reno has all three mode of operation integrated into TCP Tahoe like slow start, congestion avoidance and fast retransmit,
but in addition to that TCP Reno modify the fast retransmit mode of operation to form fast recovery mode of operation.
TCP Reno enters into fast recovery mode of operation when sender receiving three duplicate acknowledgments. 
Fast recovery operates by assuming congestion is serious in a network if the packets loss is detected due to timeout. 
If this is true then fast recovery mode set TCP Reno congestion window size to its initial value and enters into slow start mode of operation.
But if the congestion is detected due to duplicate acknowledgements then TCP Reno assume the network congestion is not serious. 
If this is true fast recovery mode set TCP Reno congestion window size to half of its current value and enters congestion avoidance
operating mode instead of slow start mode.By entering congestion avoidance mode of operation TCP Reno prevent the communication link from 
going empty after packet loss by avoiding slow start.
source:http://www.ukessays.com/essays/communications/transmission-control-protocol.php

I chose TCP reno and TCP High Speed as TCP Reno is conventional protocol which has the problem of underutilization of 
bandwidth whereas TCP highspeed overcomes this problem of Reno. Hence both the protocols are similar yet significanty different at LFN.
Hence I have selected these two protocols for my study.

4) Describe the parameters you have chosen to vary in your
experiment. Why did you choose these? How does this selection help further your stated goal?
The parameters that I have selected is link latency and capacity.
Since I want to study TCP variant on LFN and non-LFN link, I am varying bandwisth and delay such that it is<10^5(Non-LFN)
and >10^5(LFN).

5) What metrics are you going to measure in your experiment?
Why did you choose these? How does this selection help further your stated goal?
Metrics that I have selected is Throughput and cwnd.
how efficiently a protocol performs, is mainly dependent on its ability to tranfer data by utilizing the available bandwidth effectively
even at the time of congestion. Hence I am measuring throughput and cwnd to see how quickly throughput achieves its highest value and how fast 
congestion window converges.

6) For each **experimental unit** in your experiment, describe:

* The specific parameters with which this experimental unit ran
Parameters chosen are link latency and capacity.
* The names of all the data files which give results from this experimental unit. Include all of these files in this `submit` folder.
cwnd-reno-NonLFN-5ms
cwnd-highspeed-NonLFN-5ms
cwnd-cubic-NonLFN-5ms
throughput-reno-NonLFN-5ms
throughput-highspeed-NonLFN-5ms
throughput-cubic-NonLFN-5ms
cwnd-reno-LFN-40ms
cwnd-highspeed-LFN-40ms
cwnd-cubic-LFN-40ms
throughput-reno-LFN-40ms
throughput-highspeed-LFN-40ms
throughput-cubic-LFN-40ms

* The specific values of the metrics you have chosen to measure.
Metrics chosen are throughput and cwnd.

7) Describe any evidence of *interactions* you can see in the results of your experiment.

8) Briefly describe the results of your experiment (1-2 paragraphs). You may include images by putting them on an online
image hosting service, then putting the image URL in this file
using the syntax
![](http://link/to/image.png)

Result:
Performance of LFN protocol highspeed , non-LFN protocol Reno is very similar to LFN protocol that I have selected(Cubic) on non-LFN link.
But behaviour of both LFN protocol is significantly different as compared to non-LFN protocol Reno.
It is evident from the output files that highspeed and cubic achieves their maximum throughput faster as compared to reno.
and also cubic and highspeed has more throughput.
similarly, cwnd convergence of high speed and cubic is same as reno on non-LFN link.
but high speed and cubic converges faster as compared to reno on LFN link.

9) Find a published paper that studies the same TCP congestion
control variant you have chosen (it may study others as well).
Identify the paper you have chosen with a full citation, and
briefly answer the following questions about it:

 * What research question does this study seek to answer?
 * What kind of network environment was this study conducted in?
 * How representative is the above network environment of the network setting this TCP variant is designed for?
 * Does this study make some comparison to another TCP congestion
 control algorithm? If so, which, and does the author explain why these were selected?
 * What parameters does the author of this study consider? Does the
 author explain why?
 * What metrics does the author of this study consider? Does the author explain why?
 * Critique the experiment(s) in the paper. Does it make any of
 the common mistakes described in the lab lecture? Explain.
