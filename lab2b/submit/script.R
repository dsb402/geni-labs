 dat <- read.csv("lab2/submit/throughput-cubic-LFN-40ms")
 array <- c("time", "srcip", "srcport", "dstip", 
"dstport", "id", "interval", "data", "tput")
 names(dat) <- array
 dat$tcp <- "cubic"
 dat$capacity <- 100000
 dat$rtt <- 40
 dat$trial <- 1
 clubbed <- dat
 
 dat <- read.csv("lab2/submit/throughput-highspeed-LFN-40ms")
 names(dat) <- array
 dat$tcp <- "highspeed"
 dat$capacity <- 100000
 dat$rtt <- 40
 dat$trial <- 1
 
 clubbed <- rbind(clubbed, dat)
 
 dat <- read.csv("lab2/submit/throughput-reno-LFN-40ms")
 names(dat) <- array
 dat$tcp <- "reno"
 dat$capacity <- 100000
 dat$rtt <- 40
 dat$trial <- 1
 clubbed <- rbind(clubbed, dat)
 
 dat <- read.csv("lab2/submit/throughput-cubic-NonLFS-5ms")
 names(dat) <- array
 dat$tcp <- "cubic"
 dat$capacity <- 1000
 dat$rtt <- 5
 dat$trial <- 2
 clubbed <- rbind(clubbed, dat)
 
 dat <- read.csv("lab2/submit/throughput-highspeed-NonLFS-5ms")
 names(dat) <- array
 dat$tcp <- "highspeed"
 dat$capacity <- 1000
 dat$rtt <- 5
 dat$trial <- 2
 clubbed <- rbind(clubbed, dat)
 
 
 dat <- read.csv("lab2/submit/throughput-reno-NonLFN-5ms")
 names(dat) <- array
 dat$tcp <- "reno"
 dat$capacity <- 1000
 dat$rtt <- 5
 dat$trial <- 2
 clubbed <- rbind(clubbed, dat)
 
 
 clubbed$trial <- as.factor(clubbed$trial)
 clubbed$tcp <- as.factor(clubbed$tcp)
 clubbed$rtt <- as.factor(clubbed$rtt)
 
library(ggplot2)
q6 <- ggplot(clubbed)
q6 <- q6 + geom_line(aes(x=time, y=tput, colour=tcp, linetype=rtt))
ggsave("throughput.png",plot=q6)
q6



dat <- read.table("lab2/submit/cwnd_cubic_LFN_40ms.1-processed.txt")

newheader<- c("oml.ts","oml.id","oml.seq",
    "socket","state","ca.state","last.data.sent","last.data.recv",
    "snd.cwnd","snd.ssthresh","rcv.ssthresh","rtt","rttvar","snd.mss",
    "rcv.mss","rto","ato","unacked","sacked","lost","retrans","fackets",
    "total.retrans")
names(dat) <- newheader

dat$tcp <- "cubic"
dat$capacity <- 100000
dat$rtt <- 40
dat$trial <- 1

newagg <- dat


dat <- read.table("lab2/submit/cwnd_highspeed_LFN_40ms-processed.txt")
names(dat) <- newheader
dat$tcp <- "highspeed"
dat$capacity <- 100000
dat$rtt <- 40
dat$trial <- 1
newagg <- rbind(newagg, dat)

dat <- read.table("lab2/submit/cwnd_reno_LFN_40ms-processed.txt")
names(dat) <- newheader
dat$tcp <- "reno"
dat$capacity <- 100000
dat$rtt <- 40
dat$trial <- 1
newagg <- rbind(newagg, dat)

dat <- read.table("lab2/submit/cwnd_cubic_NonLFN_5ms-processed.txt")
names(dat) <- newheader
dat$tcp <- "cubic"
dat$capacity <- 1000
dat$rtt <- 5
dat$trial <- 2
newagg <- rbind(newagg, dat)


dat <- read.table("lab2/submit/cwnd_highspeed_NonLFN_5ms-processed.txt")
names(dat) <- newheader
dat$tcp <- "highspeed"
dat$capacity <- 1000
dat$rtt <- 5
dat$trial <- 2
newagg <- rbind(newagg, dat)

dat <- read.table("lab2/submit/cwnd_reno_NonLFN_5ms-processed.txt")
names(dat) <- newheader
dat$tcp <- "reno"
dat$capacity <- 1000
dat$rtt <- 5
dat$trial <- 2
newagg <- rbind(newagg, dat)

newagg$trial <- as.factor(newagg$trial)
newagg$tcp <- as.factor(newagg$tcp)
newagg$rtt <- as.factor(newagg$rtt)

library(ggplot2)

q7 <- ggplot(newagg)
q7 <- q7 + geom_line(aes(x=oml.ts, y=snd.cwnd, colour=tcp, linetype=rtt))
q7

ggsave("cwnd.png", plot=q7)






















