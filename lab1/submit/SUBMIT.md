
## Submission

Please answer the following questions in Markdown. Push your code to
BitBucket to submit this assignment.

1) The following shows the `ifconfig` command and its output for the client and server in *my* experiment. Replace these blocks with the `ifconfig` output of the client and server from *your* experiment.

```
dsb402@client:~$ sudo ifconfig
eth0      Link encap:Ethernet  HWaddr 02:6e:3b:ba:e5:cf
          inet addr:172.17.2.5  Bcast:172.31.255.255  Mask:255.240.0.0
          inet6 addr: fe80::6e:3bff:feba:e5cf/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:954 errors:0 dropped:0 overruns:0 frame:0
          TX packets:406 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:87602 (87.6 KB)  TX bytes:49289 (49.2 KB)
          Interrupt:25

eth1      Link encap:Ethernet  HWaddr 02:49:36:89:dc:ee
          inet addr:10.1.1.1  Bcast:10.1.1.255  Mask:255.255.255.0
          inet6 addr: fe80::49:36ff:fe89:dcee/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:6 errors:0 dropped:0 overruns:0 frame:0
          TX packets:8 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:384 (384.0 B)  TX bytes:900 (900.0 B)
          Interrupt:26

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:16436  Metric:1
          RX packets:4 errors:0 dropped:0 overruns:0 frame:0
          TX packets:4 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:260 (260.0 B)  TX bytes:260 (260.0 B)
```

```
dsb402@server:~$ sudo ifconfig
eth0      Link encap:Ethernet  HWaddr 02:45:43:d5:ab:bc
          inet addr:172.17.2.6  Bcast:172.31.255.255  Mask:255.240.0.0
          inet6 addr: fe80::45:43ff:fed5:abbc/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:973 errors:0 dropped:0 overruns:0 frame:0
          TX packets:486 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:91242 (91.2 KB)  TX bytes:56083 (56.0 KB)
          Interrupt:25

eth1      Link encap:Ethernet  HWaddr 02:65:ce:45:aa:a1
          inet addr:10.1.1.2  Bcast:10.1.1.255  Mask:255.255.255.0
          inet6 addr: fe80::65:ceff:fe45:aaa1/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:20 errors:0 dropped:0 overruns:0 frame:0
          TX packets:11 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:1556 (1.5 KB)  TX bytes:1138 (1.1 KB)
          Interrupt:26

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:16436  Metric:1
          RX packets:4 errors:0 dropped:0 overruns:0 frame:0
          TX packets:4 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:260 (260.0 B)  TX bytes:260 (260.0 B)
```


2)  Fill out the missing cells in the following table, using the results
of your individual experiment:

Node    | control interface name | control IP | data interface name | data IP
------- | ---------------------- | ---------- | ------------------- | --------
client  |         eth0           |172.17.2.5  |    eth1             |10.1.1.1
server  |         eth0           |172.17.2.6  |    eth1             |10.1.1.2

3)  Fill out the missing cells in the following table, using the results
of your individual experiment:

Link     |  Bandwidth (Mbits/sec)
-------- | ----------------------
control  |   3.98 Gbits/sec
data     |   100 Mbits/sec


4) What happens if you bring down the data interface of your node using the command

    sudo ifconfig DATAIFNAME down

 where `DATAIFNAME` is the name of the data interface (i.e. `eth0` or `eth1`)? What happens if you bring down the control interface of your node? Why?
Ans- When data interface is down, server can not communicate to client.On ping it gives destination unreachable.
     When control interface is down, host is disconnected from client/server and connection needs to be reset.