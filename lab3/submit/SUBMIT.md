
Lab 3: Routing and resiliency
=====================================================

Please fill in your name and net ID in the table below.

Lab Assignment | 3 - Routing and resiliency
-------------- | --------------------------------
Name           | Deepali Bhanushali
Net ID         | dsb402
Report due     | Sunday, 29 March 11:59PM


Please answer the following questions:


## Dijkstra's algorithm experiment

1. Were you able to successfully produce experiment results? If so, show a screenshot of your experiment results (topology + completed table)
Yes I was able to successfuly produce experments results.

2. How long did it take you to run this experiment, from start (create an account) to finish (getting a screenshot of results)?
It took me 15 minutes to ru this experiment, from start to finish.

3. Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
No. I did not have to do anything additional.

4. In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
It falls in degree 5 of reproducibility.

5. Given the materials and documentation provided by the experiment designers, how long do you think it would take you to set up and run the experiment if there was no web-based interface on hyperion.poly.edu?
I think it would take around 90 minutes to set up and run the experiment if there was no web-based interface on hyperion.poly.edu
## OSPF experiment

1. Were you able to successfully produce experiment results? If so, show your experiment results. You should have:
 * Traceroute from client to server with all links active
traceroute to 192.168.20.10 (192.168.20.10), 30 hops max, 60 byte packets
 1  router-1-lan5 (192.168.10.10)  0.619 ms  0.567 ms  0.534 ms
 2  router-2-lan0 (192.168.1.2)  1.128 ms  1.109 ms  1.073 ms
 3  router-3-lan2 (192.168.3.2)  1.819 ms  1.797 ms  1.764 ms
 4  server-lan6 (192.168.20.10)  2.193 ms  2.167 ms  2.160 ms
 
 
 * OSPF table on router-1 with all links active
192.168.2.0/24  [ospf(110)/20]
                > to 192.168.1.2 via eth1/eth1
192.168.3.0/24  [ospf(110)/20]
                > to 192.168.4.1 via eth2/eth2
192.168.5.0/24  [ospf(110)/20]
                > to 192.168.1.2 via eth1/eth1
192.168.20.0/24 [ospf(110)/30]
                > to 192.168.1.2 via eth1/eth1
                
 * Traceroute from client to server with the router-2 link down
traceroute to 192.168.20.10 (192.168.20.10), 30 hops max, 60 byte packets
 1  router-1-lan5 (192.168.10.10)  0.932 ms  0.888 ms  0.857 ms
 2  router-4-lan3 (192.168.4.1)  1.144 ms  1.129 ms  1.098 ms
 3  router-3-lan2 (192.168.3.2)  1.716 ms  1.690 ms  1.659 ms
 4  server-lan6 (192.168.20.10)  2.051 ms  2.025 ms  2.009 ms
 
 * OSPF table on router-1 with the router-2 link down
192.168.2.0/24  [ospf(110)/30]
                > to 192.168.4.1 via eth2/eth2
192.168.3.0/24  [ospf(110)/20]
                > to 192.168.4.1 via eth2/eth2
192.168.5.0/24  [ospf(110)/20]
                > to 192.168.1.2 via eth1/eth1
192.168.20.0/24 [ospf(110)/30]
                > to 192.168.4.1 via eth2/eth2

2. How long did it take you to run this experiment, from start (reserve resources on GENI) to finish (getting the output to put in the previous )?
It took me about 30 minutes to run the experiment from start t finish

3. Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
No. I did not have to do any additional steps beyond the documentation in order to successfully complete the experiment.

4. In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
I would rate this experiment at four degree of reproducibility.

5. Given the materials and documentation provided by the experiment designers, how long do you think it would take you to set up and run the experiment if the experiment artifacts (disk image, RSpec, etc.) were not provided for you?
I think it woud take me close to 48 hours to set up and run the experiment if the experiment artfacts were not provided to me.